﻿using Logic.Exceptions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Presentation.Middleware
{
    public class ExceptionHandlerMiddleware 
    {
        private const string _jsonContentType = "application/json";
        private readonly RequestDelegate _next;
        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context) {
            try
            {
                await _next(context);
            }
            catch (Exception ex) when (ex is LogicException)
            {
                await HandleExceptionAsync(context, ex);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception) {
            var ErrorResponse = new MiddlewareResponse<string>(null);
            if (exception is LogicException)
            {
                ErrorResponse.status = (int)HttpStatusCode.OK;
                ErrorResponse.error.message = "Logic Exception: " + exception.Message;
            }
            else
            {
                ErrorResponse.status = (int)HttpStatusCode.InternalServerError;
                ErrorResponse.error.message = "Internal Server Error" + exception.Message;
            }

            context.Response.ContentType = _jsonContentType;
            context.Response.StatusCode = ErrorResponse.status;
            return context.Response.WriteAsync(JsonConvert.SerializeObject(ErrorResponse));
        }
    }
}
