using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Data.Repository;
using Logic.Services;
using Data;
using Presentation.Middleware;
using Serilog;
using Serilog.Events;

namespace pizza_api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();

            IConfigurationSection LoggerPath = Configuration.GetSection("Logger").GetSection("LogPath");

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel
                .Information()
                .WriteTo.RollingFile(LoggerPath.Value, LogEventLevel.Information)
                .CreateLogger();
            Log.Information("Json file setup has been read: " + $"appsettings.{env.EnvironmentName}.json");
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<RepositoryContext>();
            services.AddTransient<IToppingService, ToppingService>();
            services.AddTransient<IPizzaService, PizzaService>();
            services.AddTransient<IToppingRepository, ToppingRepository>();
            services.AddTransient<IPizzaRepository, PizzaRepository>();
            services.AddTransient<IPizzaToppingRepository, PizzaToppingRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            ConfigureSwagger(services);
        }

        private static void ConfigureSwagger(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowPizzaUi",
                    builder => builder.AllowAnyOrigin()
                                      .AllowAnyHeader()
                                      .AllowAnyMethod());
            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Pizza API", Version = "v1" });
            });
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseCors("AllowPizzaUi");
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("../swagger/v1/swagger.json", "Pizza API");
            });
            app.UseMiddleware<ExceptionHandlerMiddleware>();
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
