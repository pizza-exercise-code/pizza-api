﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic.Services;
using Data.Models;
using Presentation.Middleware;
using System.Net;

namespace pizza_api.Controllers
{
    [Route("api/toppings")]
    [ApiController]
    public class ToppingController : Controller
    {
        private readonly IToppingService toppingService;
        public ToppingController(IToppingService toppingService)
        {
            this.toppingService = toppingService;
        }

        [ProducesResponseType(typeof(MiddlewareResponse<IEnumerable<Topping>>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [HttpGet]
        public async Task<IActionResult> GetToppings()
        {
            return Ok(new MiddlewareResponse<IEnumerable<Topping>>(await toppingService.GetToppings()));
        }

        [HttpGet("{toppingId}")]
        [ProducesResponseType(typeof(MiddlewareResponse<Topping>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> GetPizzaById(Guid toppingId)
        {
            return Ok(new MiddlewareResponse<Topping>(await toppingService.GetToppingById(toppingId)));
        }

        [ProducesResponseType(typeof(MiddlewareResponse<Topping>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [HttpPut("{toppingId}")]
        public async Task<IActionResult> UpdateTopping(Guid toppingId, [FromBody] Topping topping)
        {
            return Ok(new MiddlewareResponse<Topping>(await toppingService.UpdateTopping(toppingId, topping)));
        }

        [ProducesResponseType(typeof(MiddlewareResponse<Topping>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [HttpPost]
        public async Task<IActionResult> CreateTopping([FromBody] Topping topping)
        {
            return Ok(new MiddlewareResponse<Topping>(await toppingService.CreateTopping(topping)));
        }

        [ProducesResponseType(typeof(MiddlewareResponse<bool>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [HttpDelete("{toppingId}")]
        public async Task<IActionResult> DeleteTopping(Guid toppingId)
        {
            return Ok(new MiddlewareResponse<bool>(await toppingService.DeleteTopping(toppingId)));
        }
    }
}
