﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Logic.Services;
using Data.Models;
using System.Threading.Tasks;
using Presentation.Middleware;
using System.Net;
using Logic.DTO;

namespace pizza_api.Controllers
{
    [Route("api/pizzas")]
    [ApiController]
    public class PizzaController : Controller
    {
        private readonly IPizzaService pizzaService;
        public PizzaController(IPizzaService pizzaService)
        {
            this.pizzaService = pizzaService;
        }

        [ProducesResponseType(typeof(MiddlewareResponse<IEnumerable<Pizza>>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [HttpGet]
        public async Task<IActionResult> GetPizzas()
        {
            return Ok(new MiddlewareResponse<IEnumerable<Pizza>>(await pizzaService.GetPizzas()));
        }

        [ProducesResponseType(typeof(MiddlewareResponse<Pizza>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [HttpGet("{pizzaId}")]
        public async Task<IActionResult> GetPizzaById(Guid pizzaId)
        {
            return Ok(new MiddlewareResponse<Pizza>(await pizzaService.GetPizzaById(pizzaId)));
        }

        [ProducesResponseType(typeof(MiddlewareResponse<PizzaFlavor>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [HttpGet("{pizzaId}/toppings")]
        public async Task<IActionResult> GetToppingsByPizzaId(Guid pizzaId)
        {
            return Ok(new MiddlewareResponse<PizzaFlavor>(await pizzaService.GetToppingsFromPizza(pizzaId)));
        }

        [ProducesResponseType(typeof(MiddlewareResponse<PizzaFlavor>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [HttpPost("{pizzaId}/add-toppings-to-pizza")]
        public async Task<IActionResult> AddToppingToPizza(Guid pizzaId, [FromBody] List<Topping> toppings)
        {
            return Ok(new MiddlewareResponse<PizzaFlavor>(await pizzaService.AddToppingToPizza(pizzaId, toppings)));
        }

        [ProducesResponseType(typeof(MiddlewareResponse<bool>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [HttpDelete("{pizzaId}")]
        public async Task<IActionResult> DeletePizza(Guid pizzaId)
        {
            return Ok(new MiddlewareResponse<bool>(await pizzaService.DeletePizza(pizzaId)));
        }

        [ProducesResponseType(typeof(MiddlewareResponse<Pizza>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [HttpPut("{pizzaId}")]
        public async Task<IActionResult> UpdatePizza(Guid pizzaId, [FromBody] Pizza pizza)
        {
            return Ok(new MiddlewareResponse<Pizza>(await pizzaService.UpdatePizza(pizzaId, pizza)));
        }

        [ProducesResponseType(typeof(MiddlewareResponse<Pizza>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [HttpPost()]
        public async Task<IActionResult> CreatePizza([FromBody] Pizza pizza)
        {
            return Ok(new MiddlewareResponse<Pizza>(await pizzaService.CreatePizza(pizza)));
        }
    }
}
