# Pizza API
Pizza API is a web application backend developed on .NET Framework, its primary responsibility is to manage all business logic for a pizza store.

## Prerequirements

* Visual Studio 2019
* .NET Core SDK (3.1 version)
* SQL Server

## How To Run

* Open solution in Visual Studio 2019
* Set .Web project as Startup Project and build the project.
* Run the application.
