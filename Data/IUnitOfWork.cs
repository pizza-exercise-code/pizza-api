﻿using Data.Repository;

namespace Data
{
    public interface IUnitOfWork
    {
        IPizzaRepository PizzaRepository { get; }
        IToppingRepository ToppingRepository { get; }
        IPizzaToppingRepository PizzaToppingRepository { get; }
        void BeginTransaction();
        void CommitTransaction();
        void RollBackTransaction();
        void Save();
    }
}
