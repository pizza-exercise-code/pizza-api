﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Models
{
    [Table("PizzaToppings")]
    public class PizzaTopping
    {
        public Guid Id { set; get; }
        public Guid PizzaId { set; get; }
        public Guid ToppingId { set; get; }
    }
}
