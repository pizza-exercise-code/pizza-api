﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Models
{
    [Table("Toppings")]
    public class Topping
    {
        public Guid Id { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
    }
}
