﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Models
{
    [Table("Pizzas")]
    public class Pizza
    {
        public Guid Id { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
        public double Price { set; get; }
        public string ImageUrl { set; get; }
    }
}
