USE [PizzaDB]
GO

INSERT [dbo].[Pizzas] ([Id], [Name], [Description], [Price], [ImageUrl]) VALUES ('3297F0F2-35D3-4231-919D-1CFCF4035975', 'Hawaiian', 'Pizza · Hawaiian', 10,'No Image')
INSERT [dbo].[Pizzas] ([Id], [Name], [Description], [Price], [ImageUrl]) VALUES ('3297F0F2-35D3-4231-919D-1CFCF4035976', 'Peperoni', 'Pizza · Peperoni', 10,'No Image')
INSERT [dbo].[Pizzas] ([Id], [Name], [Description], [Price], [ImageUrl]) VALUES ('3297F0F2-35D3-4231-919D-1CFCF4035977', 'Irish', 'Pizza · Irish', 10,'No Image')
GO

INSERT [dbo].[Toppings] ([Id], [Name], [Description]) VALUES ('3297F0F2-35D3-4231-919D-1CFCF4035978', 'Ham', 'Topping - Ham')
INSERT [dbo].[Toppings] ([Id], [Name], [Description]) VALUES ('3297F0F2-35D3-4231-919D-1CFCF4035979', 'Pineapple', 'Topping - Pineapple')
INSERT [dbo].[Toppings] ([Id], [Name], [Description]) VALUES ('3297F0F2-35D3-4231-919D-1CFCF4035980', 'Cabbage', 'Topping - Cabbage')
GO

INSERT [dbo].[PizzaToppings] ([Id], [PizzaId], [ToppingId]) VALUES ('3297F0F2-35D3-4231-919D-1CFCF4035981', '3297F0F2-35D3-4231-919D-1CFCF4035975', '3297F0F2-35D3-4231-919D-1CFCF4035978')
INSERT [dbo].[PizzaToppings] ([Id], [PizzaId], [ToppingId]) VALUES ('3297F0F2-35D3-4231-919D-1CFCF4035982', '3297F0F2-35D3-4231-919D-1CFCF4035975', '3297F0F2-35D3-4231-919D-1CFCF4035979')
INSERT [dbo].[PizzaToppings] ([Id], [PizzaId], [ToppingId]) VALUES ('3297F0F2-35D3-4231-919D-1CFCF4035983', '3297F0F2-35D3-4231-919D-1CFCF4035976', '3297F0F2-35D3-4231-919D-1CFCF4035979')
INSERT [dbo].[PizzaToppings] ([Id], [PizzaId], [ToppingId]) VALUES ('3297F0F2-35D3-4231-919D-1CFCF4035984', '3297F0F2-35D3-4231-919D-1CFCF4035977', '3297F0F2-35D3-4231-919D-1CFCF4035980')
GO