USE [PizzaDB]
GO
CREATE TABLE [dbo].[Pizzas](
	[Id] UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
	[Name] [varchar] (50) NOT NULL,
	[Description] [nvarchar] (250),
	[Price] [float],
	[ImageUrl] [nvarchar] (250)
)

GO
CREATE TABLE [dbo].[Toppings] (
	[Id] UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
	[Name] [varchar] (50) NOT NULL,
	[Description] [nvarchar] (250),
)

GO
CREATE TABLE [dbo].[PizzaToppings] (
	[Id] UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
    [PizzaId] UNIQUEIDENTIFIER NOT NULL,
    [ToppingId] UNIQUEIDENTIFIER NOT NULL,
    FOREIGN KEY (PizzaId) REFERENCES [dbo].[Pizzas](Id),
    FOREIGN KEY (ToppingId) REFERENCES [dbo].[Toppings](Id)
);
