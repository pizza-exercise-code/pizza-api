﻿using Data.Models;
using Data.Repository.Base;

namespace Data.Repository
{
    public class PizzaRepository : RepositoryBase<Pizza>, IPizzaRepository
    {
        public PizzaRepository(RepositoryContext repositoryContext) : base (repositoryContext) {}
    }
}
