﻿using Data.Models;
using Data.Repository.Base;

namespace Data.Repository
{
    public interface IPizzaToppingRepository : IRepositoryBase<PizzaTopping> { }
}
