﻿using Data.Models;
using Data.Repository.Base;

namespace Data.Repository
{
    public interface IToppingRepository : IRepositoryBase<Topping> { }
}
