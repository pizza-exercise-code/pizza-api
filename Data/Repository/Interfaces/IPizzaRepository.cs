﻿using Data.Models;
using Data.Repository.Base;

namespace Data.Repository
{
    public interface IPizzaRepository : IRepositoryBase<Pizza> { }
}
