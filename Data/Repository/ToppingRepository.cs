﻿using Data.Models;
using Data.Repository.Base;

namespace Data.Repository
{
    public class ToppingRepository : RepositoryBase<Topping>, IToppingRepository
    {
        public ToppingRepository(RepositoryContext repositoryContext) : base(repositoryContext) { }
    }
}
