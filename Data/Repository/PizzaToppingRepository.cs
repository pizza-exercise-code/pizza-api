﻿using Data.Models;
using Data.Repository.Base;

namespace Data.Repository
{
    public class PizzaToppingRepository : RepositoryBase<PizzaTopping>, IPizzaToppingRepository
    {
        public PizzaToppingRepository(RepositoryContext repositoryContext) : base (repositoryContext) {}
    }
}
