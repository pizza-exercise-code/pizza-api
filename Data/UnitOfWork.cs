﻿using System;

using Microsoft.EntityFrameworkCore;

using Data.Repository;

namespace Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly RepositoryContext repositoryContext;
        private readonly IPizzaRepository pizzaRepository;
        private readonly IToppingRepository toppginsRepository;
        private readonly IPizzaToppingRepository pizzaToppingRepository;
        
        public UnitOfWork(RepositoryContext repositoryContext)
        {
            this.repositoryContext = repositoryContext;
            pizzaRepository = new PizzaRepository(repositoryContext);
            toppginsRepository = new ToppingRepository(repositoryContext);
            pizzaToppingRepository = new PizzaToppingRepository(repositoryContext);
        }

        public void BeginTransaction()
        {
            repositoryContext.Database.BeginTransaction();
        }

        public void CommitTransaction()
        {
            repositoryContext.Database.CommitTransaction();
        }

        public void RollBackTransaction()
        {
            repositoryContext.Database.RollbackTransaction();
        }

        public void Save()
        {
            try
            {
                BeginTransaction();
                repositoryContext.SaveChanges();
                CommitTransaction();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                RollBackTransaction();
                throw ex;
            }
            catch (DbUpdateException ex)
            {
                RollBackTransaction();
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IPizzaRepository PizzaRepository {
            get { return pizzaRepository; }
        }

        public IToppingRepository ToppingRepository {
            get { return toppginsRepository; }
        }

        public IPizzaToppingRepository PizzaToppingRepository { 
            get { return pizzaToppingRepository; }
        }
    }
}
