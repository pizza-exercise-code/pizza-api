﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Linq;

using Microsoft.EntityFrameworkCore;

namespace Data.Repository.Base
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        public RepositoryContext repositoryContext { get; set; }
        public RepositoryBase(RepositoryContext repositoryContext)
        {
            this.repositoryContext = repositoryContext;
        }
        public async Task<T> CreateAsync(T entity)
        {
            await repositoryContext.Set<T>().AddAsync(entity);
            await repositoryContext.SaveChangesAsync();
            return entity;
        }

        public async Task<bool> DeleteAsync(T entity)
        {
            repositoryContext.Set<T>().Remove(entity);
            await repositoryContext.SaveChangesAsync();
            return true;
        }

        public async Task<IEnumerable<T>> FindAllAsync()
        {
            return await repositoryContext.Set<T>().ToListAsync();
        }

        public async Task<IEnumerable<T>> FindByConditionAsync(Expression<Func<T, bool>> expression)
        {
            return await repositoryContext.Set<T>().Where(expression).ToListAsync();
        }
         
        public async Task<T> UpdateAsync(T entity)
        {
            repositoryContext.Entry(entity).State = EntityState.Modified;
            await repositoryContext.SaveChangesAsync();
            return entity;
        }

        public async Task<T> FindOneByConditionAsync(Expression<Func<T, bool>> expression)
        {
            return await repositoryContext.Set<T>().Where(expression).FirstOrDefaultAsync();
        }

        public async Task<bool> DeleteRangeAsync(List<T> entity)
        {
            repositoryContext.Set<T>().RemoveRange(entity);
            await repositoryContext.SaveChangesAsync();
            return true;
        }
        public async Task<List<T>> CreateRangeAsync(List<T> entity)
        {
            await repositoryContext.Set<T>().AddRangeAsync(entity);
            await repositoryContext.SaveChangesAsync();
            return entity;
        }
    }
}
