﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Models;

namespace Logic.Services
{
    public interface IToppingService
    {
        Task<Topping> GetToppingById(Guid toppginId);
        Task<IEnumerable<Topping>> GetToppings();
        Task<Topping> CreateTopping(Topping topping);
        Task<bool> DeleteTopping(Guid toppingId);
        Task<Topping> UpdateTopping(Guid toppingId, Topping updateTopping);
    }
}
