﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Models;
using Logic.DTO;

namespace Logic.Services
{
    public interface IPizzaService
    {
        Task<Pizza> GetPizzaById(Guid id);
        Task<IEnumerable<Pizza>> GetPizzas();
        Task<Pizza> CreatePizza(Pizza pizza);
        Task<bool> DeletePizza(Guid pizzaId);
        Task<Pizza> UpdatePizza(Guid pizzaId, Pizza pizza);
        Task<PizzaFlavor> GetToppingsFromPizza(Guid pizzaId);
        Task<PizzaFlavor> AddToppingToPizza(Guid pizzaId, List<Topping> toppings);
    }
}
