﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Models;
using System.Linq;
using Data;
using Logic.Exceptions;
using Serilog;

namespace Logic.Services
{
    public class ToppingService : IToppingService
    {
        private readonly IUnitOfWork uow;
        public ToppingService(IUnitOfWork uow)
        {
            this.uow = uow;
        }

        public async Task<Topping> CreateTopping(Topping topping)
        {
            try
            {
                topping.Id = new Guid();
                await uow.ToppingRepository.CreateAsync(topping);
                return topping;
            }
            catch (Exception ex)
            {
                string message = $"There is problem with the data providen | ToppingService -> GetPizzaFromRepository(){Environment.NewLine}Message: {ex.Message}{Environment.NewLine}";
                Log.Error(ex, $"{message}{Environment.NewLine}StackTrace: {Environment.NewLine}");
                throw new LogicException(ex.Message, ex.InnerException);
            }
        }

        public async Task<bool> DeleteTopping(Guid toppingId)
        {
            Topping toppingToDelete = await GetToppingFromRepository(toppingId);
            IEnumerable<PizzaTopping> pizzaToppings = await uow.PizzaToppingRepository.FindByConditionAsync(p => p.ToppingId == toppingId);
            List<PizzaTopping> pizzaToppingsToDlete = new List<PizzaTopping>(pizzaToppings);
            bool result = await uow.PizzaToppingRepository.DeleteRangeAsync(pizzaToppingsToDlete);
            if (result)
            {
                return await uow.ToppingRepository.DeleteAsync(toppingToDelete);
            }
            else
            {
                throw new LogicException($"There is a problem deleting the topping with the id {toppingId}");
            }
        }

        public async Task<Topping> GetToppingById(Guid toppingId)
        {
            return await GetToppingFromRepository(toppingId);
        }

        public async Task<IEnumerable<Topping>> GetToppings()
        {
            return await uow.ToppingRepository.FindAllAsync();
        }

        public async Task<Topping> UpdateTopping(Guid toppingId, Topping updateTopping)
        {
            Topping toppingToUpdate = await GetToppingFromRepository(toppingId);
            toppingToUpdate.Description = updateTopping.Description;
            toppingToUpdate.Name = updateTopping.Name;
            return await uow.ToppingRepository.UpdateAsync(toppingToUpdate);
        }

        private async Task<Topping> GetToppingFromRepository(Guid pizzaId)
        {
            try
            {
                IEnumerable<Topping> toppings = await uow.ToppingRepository.FindByConditionAsync(p => p.Id == pizzaId);
                Topping toppingResult = toppings.FirstOrDefault();
                if (toppingResult == null)
                {
                    throw new LogicException($"Topping with ID {pizzaId} was not found");
                }
                return toppingResult;
            }
            catch (Exception ex)
            {
                string message = $"Can not find the Topping with id {pizzaId} | ToppingService -> GetToppingFromRepository(){Environment.NewLine}Message: {ex.Message}{Environment.NewLine}";
                Log.Error(ex, $"{message}{Environment.NewLine}StackTrace: {Environment.NewLine}");
                throw new LogicException(ex.Message, ex.InnerException);
            }
        }
    }
}
