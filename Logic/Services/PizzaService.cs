﻿using Data.Models;
using System;
using System.Collections.Generic;
using Data;
using System.Threading.Tasks;
using System.Linq;
using Logic.Exceptions;
using Serilog;
using Logic.DTO;

namespace Logic.Services
{
    public class PizzaService : IPizzaService
    {
        private readonly IUnitOfWork uow;
        public PizzaService(IUnitOfWork uow)
        {
            this.uow = uow;
        }

        public async Task<Pizza> CreatePizza(Pizza pizza)
        {
            try
            {
                pizza.Id = new Guid();
                Pizza resposne = await uow.PizzaRepository.CreateAsync(pizza);
                return resposne;
            }
            catch (Exception ex)
            {
                string message = $"There is problem with the data providen | PizzaService -> CreatePizza(){Environment.NewLine}Message: {ex.Message}{Environment.NewLine}";
                Log.Error(ex, $"{message}{Environment.NewLine}StackTrace: {Environment.NewLine}");
                throw new LogicException(ex.Message, ex.InnerException);
            }
        }

        public async Task<bool> DeletePizza(Guid pizzaId)
        {
            Pizza pizzaToDelete = await GetPizzaFromRepository(pizzaId);
            IEnumerable < PizzaTopping > pizzaToppings = await uow.PizzaToppingRepository.FindByConditionAsync(p => p.PizzaId == pizzaId);
            List<PizzaTopping> pizzaToppingsToDlete = new List<PizzaTopping>(pizzaToppings);
            bool result = await uow.PizzaToppingRepository.DeleteRangeAsync(pizzaToppingsToDlete);
            if (result)
            {
                return await uow.PizzaRepository.DeleteAsync(pizzaToDelete);
            }
            else {
                throw new LogicException($"There is a problem deleting the pizza with the id {pizzaId}");
            }
        }

        public async Task<Pizza> GetPizzaById(Guid pizzaId)
        {
            Pizza pizza = await GetPizzaFromRepository(pizzaId);
            return pizza;
        }

        public async Task<IEnumerable<Pizza>> GetPizzas()
        {
            return await uow.PizzaRepository.FindAllAsync();
        }

        public async Task<PizzaFlavor> GetToppingsFromPizza(Guid pizzaId)
        {
            Pizza pizzaToSearch = await GetPizzaFromRepository(pizzaId);
            List<Topping> toppings = new List<Topping>();
            IEnumerable<PizzaTopping> pizzaToppings = await uow.PizzaToppingRepository.FindByConditionAsync(p => p.PizzaId == pizzaToSearch.Id);
            foreach (PizzaTopping pizzaTopping in pizzaToppings)
            {
                Topping toppingFind = await uow.ToppingRepository.FindOneByConditionAsync(t => t.Id == pizzaTopping.ToppingId);
                toppings.Add(toppingFind);
            }
            PizzaFlavor pizzaFlavor = new PizzaFlavor()
            {
                Pizza = pizzaToSearch,
                Toppings = toppings
            };
            return pizzaFlavor;
        }

        public async Task<Pizza> UpdatePizza(Guid pizzaId, Pizza pizzaUpdate)
        {
            Pizza pizzaToUpdate = await GetPizzaFromRepository(pizzaId);
            pizzaToUpdate.ImageUrl = pizzaUpdate.ImageUrl;
            pizzaToUpdate.Description = pizzaUpdate.Description;
            pizzaToUpdate.Name = pizzaUpdate.Name;
            pizzaToUpdate.Price = pizzaUpdate.Price;
            await uow.PizzaRepository.UpdateAsync(pizzaToUpdate);
            return pizzaToUpdate;
        }

        private async Task<Pizza> GetPizzaFromRepository(Guid pizzaId) 
        {
            try
            {
                IEnumerable<Pizza> pizzas = await uow.PizzaRepository.FindByConditionAsync(p => p.Id == pizzaId);
                Pizza pizzaResult = pizzas.FirstOrDefault();
                if (pizzaResult == null)
                {
                    throw new LogicException($"Pizza with ID {pizzaId} was not found");
                }
                return pizzas.FirstOrDefault();
            }
            catch (Exception ex)
            {
                string message = $"Can not find the Pizza with id {pizzaId} | PizzaService -> GetPizzaFromRepository(){Environment.NewLine}Message: {ex.Message}{Environment.NewLine}";
                Log.Error(ex, $"{message}{Environment.NewLine}StackTrace: {Environment.NewLine}");
                throw new LogicException(ex.Message, ex.InnerException);
            }
        }

        public async Task<PizzaFlavor> AddToppingToPizza(Guid pizzaId, List<Topping> toppings)
        {
            try
            {
                await GetPizzaFromRepository(pizzaId);
                IEnumerable<PizzaTopping> toppingsToReplace = await uow.PizzaToppingRepository.FindByConditionAsync(t => t.PizzaId == pizzaId);
                List<PizzaTopping> pizzaToppings = new List<PizzaTopping>();
                await uow.PizzaToppingRepository.DeleteRangeAsync(toppingsToReplace.ToList());
                foreach (Topping topping in toppings)
                {
                    Topping toppingExist = await uow.ToppingRepository.FindOneByConditionAsync(t => t.Id == topping.Id);
                    if (toppingExist != null)
                    {
                        PizzaTopping pizzaTopping = new PizzaTopping()
                        {
                            Id = new Guid(),
                            PizzaId = pizzaId,
                            ToppingId = topping.Id
                        };
                        pizzaToppings.Add(pizzaTopping);
                    }
                    else
                    {
                        throw new LogicException($"The topping with the Id {topping.Id} does not exist");
                    }
                }
                await uow.PizzaToppingRepository.CreateRangeAsync(pizzaToppings);
                return await GetToppingsFromPizza(pizzaId);
            }
            catch (Exception ex)
            {
                string message = $"Was not possible to add toppings to pizza with {pizzaId} | PizzaService -> AddToppingToPizza(){Environment.NewLine}Message: {ex.Message}{Environment.NewLine}";
                Log.Error(ex, $"{message}{Environment.NewLine}StackTrace: {Environment.NewLine}");
                throw new LogicException(ex.Message, ex.InnerException);
            }
        }
    }
}
