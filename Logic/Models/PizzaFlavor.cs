﻿using System;
using System.Collections.Generic;
using Data.Models;

namespace Logic.DTO
{
    public class PizzaFlavor
    {
        public Pizza Pizza { get; set; }
        public IEnumerable<Topping> Toppings { get; set; }
    }
}
