using Data.Models;
using Moq;
using NUnit.Framework;
using Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logic.Services;
using System;
using Logic.Exceptions;

namespace Test
{
    public class PizzaServiceTest
    {
        private Mock<IUnitOfWork> uow;
        private List<Pizza> pizzas;
        [SetUp]
        public void Setup()
        {
            this.uow = new Mock<IUnitOfWork>();
            pizzas = new List<Pizza>
            {
                new Pizza() { Description = "Pizza 1", Id = new Guid("3297F0F2-35D3-4231-919D-1CFCF4035977"), ImageUrl = "No Image", Name = "H", Price = 12 },
                new Pizza() { Description = "Pizza 2", Id = new Guid("16BE1C0C-D8AC-4373-EAED-08DA45A9AEE2"), ImageUrl = "No Image", Name = "B", Price = 12 },
                new Pizza() { Description = "Pizza 3", Id = new Guid("16BE1C0C-D8AC-4373-EAED-08DA45A9AEE3"), ImageUrl = "No Image", Name = "C", Price = 12 }
            };
            IEnumerable<Pizza> pizzasEmun = pizzas;
            uow.Setup(a => a.PizzaRepository.FindAllAsync()).Returns(Task.FromResult(pizzasEmun));
            uow.Setup(a => a.PizzaRepository.CreateAsync(pizzas[0])).Returns(Task.FromResult(pizzas[0]));
        }

        [Test]
        public async Task TestGetAllPizzas()
        {
            var pizzaService = new PizzaService(uow.Object);
            IEnumerable<Pizza> pizzasResult = await pizzaService.GetPizzas();
            Assert.AreEqual(3, pizzasResult.Count());
        }

        [Test]
        public async Task TestCreatePizza()
        {
            var pizzaService = new PizzaService(uow.Object);
            Pizza pizzasResult = await pizzaService.CreatePizza(pizzas[0]);
            Assert.AreEqual(pizzas[0], pizzasResult);
        }

        [Test]
        public void TestLogicException()
        {
            var pizzaService = new PizzaService(uow.Object);
            Assert.ThrowsAsync<LogicException>(() => pizzaService.GetPizzaById(new Guid("3297F0F2-1233-1233-919D-1CFCF4035977")));
        }
    }
}